﻿using PrismEntity.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismEntity.Repository.Persistence.Configurations
{
    class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {

            HasIndex(c => c.Email).IsUnique(true);
            HasIndex(c => c.Username).IsUnique(true);

            Property(c => c.Email)
                .IsRequired()
                .HasMaxLength(255);

            Property(c => c.Username)
                .IsRequired()
                .HasMaxLength(100);

            Property(c => c.Password)
                .IsRequired()
                .HasMaxLength(25);

            Property(c => c.CreatedAt)
                .HasColumnType("datetime2");

        }
    }
}
