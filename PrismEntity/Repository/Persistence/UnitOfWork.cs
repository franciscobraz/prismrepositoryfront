﻿using PrismEntity.Repository.Interfaces;
using PrismEntity.Repository.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismEntity.Repository.Persistence
{
    class UnitOfWork : IUnitOfWork
    {

        private readonly ApplicationContext _context;
        public IUserRepository Users { get; private set; }

        public UnitOfWork(ApplicationContext context)
        {
            _context = context;
            Users = new UserRepository(_context);
        }


        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
