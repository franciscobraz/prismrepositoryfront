﻿using PrismEntity.Views;
using Prism.Ioc;
using Prism.Modularity;
using System.Windows;
using PrismEntity.Repository.Interfaces;
using PrismEntity.Repository.Persistence;

namespace PrismEntity
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<UsersListControl>("UsersList");
            containerRegistry.RegisterForNavigation<UsersCreateControl>("UsersCreate");
            containerRegistry.RegisterInstance(typeof(IUnitOfWork), new UnitOfWork(new ApplicationContext()));
        }
    }
}
