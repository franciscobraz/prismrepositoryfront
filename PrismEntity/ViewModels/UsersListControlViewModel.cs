﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using PrismEntity.Models;
using PrismEntity.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace PrismEntity.ViewModels
{
    public class UsersListControlViewModel : BindableBase, INavigationAware
    {

        private IUnitOfWork _unit { get; set; }

        private User _selectedUser;
        public User SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                SetProperty(ref _selectedUser, value);
                RaisePropertyChanged("IsSelected");
            }
        }

        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get { return _users; }
            set { SetProperty(ref _users, value); }
        }

        public Visibility IsSelected
        {
            get { return (SelectedUser != null) ? Visibility.Visible : Visibility.Hidden; }

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return false;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {

        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            //var parameters = navigationContext.Parameters["users"];
            //if(parameters != null)
            //{
            //    Users = parameters as ObservableCollection<User>;
            //}
        }
        public UsersListControlViewModel(IUnitOfWork unit)
        {
            Users = new ObservableCollection<User>();
            _unit = unit;
            Users = new ObservableCollection<User>(_unit.Users.GetAll().ToList());
            // TODO: Fetch Users from database
        }


    }
}
