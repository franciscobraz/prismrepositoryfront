﻿using PrismEntity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismEntity.Repository.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        // TODO: Make the Specifics Resusable User Methods
    }
}
