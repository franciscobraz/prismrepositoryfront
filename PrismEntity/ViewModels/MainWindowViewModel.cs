﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using PrismEntity.Views;

namespace PrismEntity.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "Prism Application";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private IRegionManager _regionManager;
        public IRegionManager RegionManager
        {
            get { return _regionManager; }
            set { SetProperty(ref _regionManager, value); }
        }

        public DelegateCommand<string> NavigateToPage { get; set; }

        private bool CanExecuteNavigate(string parameter)
        {
            return true;
        }

        private void Navigate(string parameter)
        {
            _regionManager.RequestNavigate("ContentRegion", parameter);
        }

        public MainWindowViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
            NavigateToPage = new DelegateCommand<string>(Navigate, CanExecuteNavigate);
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(UsersListControl));
        }
    }
}
